package com.example.offlinecaching.ui

import androidx.lifecycle.*
import com.example.offlinecaching.api.RestaurantApi
import com.example.offlinecaching.db.MainRepository
import com.example.offlinecaching.model.Restaurant
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repo:MainRepository
) :ViewModel(){

    val restaurant = repo.getRestaurant().asLiveData()

}