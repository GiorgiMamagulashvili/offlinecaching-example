package com.example.offlinecaching.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.offlinecaching.adapter.RestaurantAdapter
import com.example.offlinecaching.databinding.ActivityMainBinding
import com.example.offlinecaching.util.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    private val mainViewModel: MainViewModel by viewModels()
    private val restaurantAdapter: RestaurantAdapter by lazy { RestaurantAdapter() }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.apply    {
            rvRestaurant.apply {
                adapter = restaurantAdapter
                layoutManager = LinearLayoutManager(this@MainActivity)
            }
            mainViewModel.restaurant.observe(this@MainActivity, Observer { result ->
                restaurantAdapter.submitList(result.data)
                progress.isVisible = result is Resource.Loading && result.data.isNullOrEmpty()
                errorTextView.isVisible = result is Resource.Error && result.data.isNullOrEmpty()
                errorTextView.text = result.error?.localizedMessage
            })
        }

    }
}