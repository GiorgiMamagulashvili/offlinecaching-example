package com.example.offlinecaching.di

import android.content.Context
import androidx.room.Room
import com.example.offlinecaching.api.RestaurantApi
import com.example.offlinecaching.db.RestaurantDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideRetrofit():Retrofit =
        Retrofit.Builder()
            .baseUrl(RestaurantApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    @Singleton
    fun providesApi(retrofit: Retrofit):RestaurantApi =
        retrofit.create(RestaurantApi::class.java)

    @Provides
    @Singleton
    fun provideDB(
        @ApplicationContext app:Context
    )=Room.databaseBuilder(
        app,
        RestaurantDatabase::class.java,
        "restaurant_database"
    ).build()

}