package com.example.offlinecaching.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "restaurant")
data class Restaurant(
    @PrimaryKey val name:String,
    val logo:String,
    val type :String,
    val address:String
)