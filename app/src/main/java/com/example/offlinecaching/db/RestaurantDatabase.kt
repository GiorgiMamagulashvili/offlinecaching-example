package com.example.offlinecaching.db

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.offlinecaching.model.Restaurant

@Database(
    entities = [Restaurant::class],
    version = 1
)
abstract class RestaurantDatabase:RoomDatabase() {

    abstract fun getDao():Dao
}