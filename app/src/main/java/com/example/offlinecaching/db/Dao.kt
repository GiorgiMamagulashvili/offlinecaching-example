package com.example.offlinecaching.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.offlinecaching.model.Restaurant
import dagger.multibindings.IntoSet
import kotlinx.coroutines.flow.Flow


@Dao
interface Dao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRestaurant(restaurant:List<Restaurant>)

    @Query("DELETE FROM restaurant")
    suspend fun deleteAll()

    @Query("SELECT * FROM restaurant")
    fun getAllRestaurant(): Flow<List<Restaurant>>
}