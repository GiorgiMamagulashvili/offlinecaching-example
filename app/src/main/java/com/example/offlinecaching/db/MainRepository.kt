package com.example.offlinecaching.db

import androidx.room.withTransaction
import com.example.offlinecaching.api.RestaurantApi
import com.example.offlinecaching.util.networkBoundResource
import kotlinx.coroutines.delay
import javax.inject.Inject

class MainRepository @Inject constructor(
    private val api:RestaurantApi,
    private val db:RestaurantDatabase
) {
    val dao = db.getDao()
    fun getRestaurant() = networkBoundResource(
        query = {
            dao.getAllRestaurant()
        },
        fetch = {
            delay(2000)
            api.getRestaurant()
        },
        saveFetchResult = { restaurant ->
            db.withTransaction {
                dao.insertRestaurant(restaurant)
                dao.getAllRestaurant()
            }

        }
    )
}